# Translation for Quinb
# Copyright (C) 2020 DeepDaikon
# This file is distributed under the same license as Quinb.
# DeepDaikon <deepdaikon@tuta.io>, 2020, 2022.
# Swann Fournial <s.fournial@protonmail.com>, 2021.
# J. Lavoie <j.lavoie@net-c.ca>, 2022.
# iiitomo <yannubuntu@protonmail.com>, 2023.
msgid ""
msgstr "Project-Id-Version: \nPOT-Creation-Date: \nPO-Revision-Date: 2023-12-16 21:16+0000\nLast-Translator: iiitomo <yannubuntu@protonmail.com>\nLanguage-Team: French <https://translate.deepdaikon.xyz/projects/quinb/games/fr/>\nLanguage: fr\nMIME-Version: 1.0\nContent-Type: text/plain; charset=UTF-8\nContent-Transfer-Encoding: 8bit\nPlural-Forms: nplurals=2; plural=n > 1;\nX-Generator: Weblate 4.4.2\n"

#: lib/game/games/count_the_animals.dart:
msgid "What is the most frequent animal?"
msgstr "Quel est l'animal le plus fréquent ?"

#: lib/game/games/count_the_animals.dart:
msgid "How many %s are there?"
msgstr "Combien de %s y a-t-il ?"

#: lib/game/games/count_the_animals.dart:
msgid "Are there more %s than %s?"
msgstr "Y a-t-il plus de %s que de %s ?"

#: lib/game/games/count_the_animals.dart:
msgid "Is there only one %s?"
msgstr "Y a-t-il un seul %s ?"

#: lib/game/games/count_the_animals.dart:
msgid "Are there %s %s?"
msgstr "Y a-t-il %s %s ?"

#: lib/game/games/remember.dart:
msgid "What animal was the %s one you saw?"
msgstr "Quel animal était le %s que tu as vu ?"

#: lib/game/games/remember.dart:
msgid "What animal was the %s one you heard?"
msgstr "Quel animal était le %s que tu as entendu ?"

#: lib/game/games/remember.dart:
msgid "What animal was the %s one you read?"
msgstr "Quel animal était le %s que tu as lu ?"

#: lib/game/games/remember.dart:
msgid "What color was the %s figure?"
msgstr "De quelle couleur était la %s figure ?"

#: lib/game/games/remember.dart:
msgid "What letter was in the %s figure?"
msgstr "Quelle lettre était dans la %s figure ?"

#: lib/game/games/remember.dart:
msgid "1st"
msgstr "1er"

#: lib/game/games/remember.dart:
msgid "2nd"
msgstr "2e"

#: lib/game/games/remember.dart:
msgid "3rd"
msgstr "3e"

#: lib/game/games/remember.dart:
msgid "4th"
msgstr "4e"

#: lib/game/games/remember.dart:
msgid "5th"
msgstr "5e"

#: lib/game/games/remember.dart:
msgid "6th"
msgstr "6e"

#: lib/game/games/what_color.dart:
msgid "What color is the text?"
msgstr "De quelle couleur est le texte ?"

#: lib/game/games/what_color.dart:
msgid "What color is the background?"
msgstr "De quelle couleur est l'arrière-plan ?"

#: lib/game/games/what_color.dart:
msgid "What color is written?"
msgstr "Quelle couleur est écrite ?"

#: lib/game/games/shortest_vibration.dart:
msgid "First"
msgstr "Première"

#: lib/game/games/shortest_vibration.dart:
msgid "Second"
msgstr "Deuxième"

#: lib/game/games/shortest_vibration.dart:
msgid "Third"
msgstr "Troisième"

#: lib/game/games/shortest_vibration.dart:
msgid "Fourth"
msgstr "Quatrième"

#: lib/game/games/fast_finger.dart:
msgid "There was no vibration!"
msgstr "Il n'y a pas eu de vibration !"

#: lib/game/games/fast_finger.dart:
msgid "You haven't tapped!"
msgstr "Vous n'avez pas appuyé !"

#: lib/game/games/fast_finger.dart:
msgid "Tap as soon as you feel a vibration"
msgstr "Appuyez dès que vous sentez une vibration"

#: lib/game/games/recognize.dart:
msgid "Tap here to listen to the sample"
msgstr "Touchez pour écouter l'échantillon"

#: lib/game/games/recognize.dart:
msgid "Tap here to start"
msgstr "Touchez ici pour commencer"

#: lib/game/games/recognize.dart:
msgid "Tap when you hear the previous sequence"
msgstr "Touchez quand vous entendez la séquence précédente"

#: lib/game/games/recognize.dart:
msgid "Tap when you hear the previous sound"
msgstr "Touchez quand vous entendez le son précédent"

#: lib/game/games/countdown.dart:
msgid "Is the countdown over?"
msgstr "Le compte à rebours est fini ?"

#: lib/game/games/countdown.dart:
msgid "Too soon!"
msgstr "Trop tôt !"

#: lib/game/games/countdown.dart:
msgid "Good!"
msgstr "Bien !"

#: lib/game/games/animal_farm.dart:
msgid "Tap when you hear a %s"
msgstr "Touchez quand vous entendez un %s"

#: lib/game/utils/option.dart:
msgid "Tap!"
msgstr "Toucher !"

#: lib/game/utils/option.dart:
msgid "Yes!"
msgstr "Oui !"

#: lib/game/utils/emoji.dart:
msgid "cat"
msgstr "chat"

#: lib/game/utils/emoji.dart:
msgid "duck"
msgstr "canard"

#: lib/game/utils/emoji.dart:
msgid "dog"
msgstr "chien"

#: lib/game/utils/colors.dart:
msgid "Blue"
msgstr "Bleu"

#: lib/game/utils/colors.dart:
msgid "Red"
msgstr "Rouge"

#: lib/game/utils/colors.dart:
msgid "Green"
msgstr "Vert"

#: lib/game/utils/colors.dart:
msgid "Yellow"
msgstr "Jaune"

#: lib/game/utils/colors.dart:
msgid "Purple"
msgstr "Violet"

#: lib/game/utils/colors.dart:
msgid "Black"
msgstr "Noir"

#: lib/game/utils/colors.dart:
msgid "Brown"
msgstr "Marron"

#: lib/game/game.dart:
msgid "Too many rounds"
msgstr "Trop de tours"

#: lib/game/game.dart:
msgid "You lost!"
msgstr "Vous avez perdu !"

#: lib/game/game.dart:
msgid "Too many mistakes."
msgstr "Trop d'erreurs."

#: lib/game/game.dart:
msgid "%s won!"
msgstr "%s a gagné !"

#: lib/game/game.dart:
msgid "Nobody won!"
msgstr "Personne n'a gagné !"

#: lib/game/game.dart:
msgid "You won!"
msgstr "Vous avez gagné !"

#: lib/game/game.dart:
msgid "Pause"
msgstr "Pause"

#: lib/game/game.dart:
msgid "Are you sure you want to quit this game?"
msgstr "Êtes-vous sûr·e de vouloir quitter cette partie ?"

#: lib/game/game.dart:
msgid "Are you sure you want to restart this game?"
msgstr "Êtes-vous sûr·e de vouloir recommencer le jeu ?"

#: lib/resources/game_list.dart:
msgid "Animal coherence"
msgstr "Cohérence des animaux"

#: lib/resources/game_list.dart:
msgid "Tap the screen if what you see is what you hear"
msgstr "Appuyez sur l'écran si ce que vous voyez correspond à ce que vous voyez"

#: lib/resources/game_list.dart:
msgid "Is it correct?"
msgstr "Est-ce correct ?"

#: lib/resources/game_list.dart:
msgid "Tap the screen when the animal you see, the animal you read, and the animal you hear are all the same"
msgstr "Appuyez quand l'animal que vous voyez, l'animal que vous lisez et l'animal que vous entendez sont le même"

#: lib/resources/game_list.dart:
msgid "Animal farm"
msgstr "La ferme des animaux"

#: lib/resources/game_list.dart:
msgid "Tap when you hear the correct animal"
msgstr "Touchez quand vous entendez le bon animal"

#: lib/resources/game_list.dart:
msgid "Tap the screen when you hear the correct animal during a sequence of sounds"
msgstr "Touchez ensuite l'écran quand vous entendez le bon animal pendant une séquence sonore"

#: lib/resources/game_list.dart:
msgid "Ascending order"
msgstr "Ordre croissant"

#: lib/resources/game_list.dart:
msgid "Tap when the list of numbers is in order"
msgstr "Appuyez quand la liste de nombres est dans l'ordre"

#: lib/resources/game_list.dart:
msgid "Is it in ascending order?"
msgstr "Est-ce dans l'ordre croissant ?"

#: lib/resources/game_list.dart:
msgid "Tap when the list of numbers is in ascending order"
msgstr "Appuyez quand la liste de nombres est dans l'ordre croissant"

#: lib/resources/game_list.dart:
msgid "Audio sequence"
msgstr "Séquence sonore"

#: lib/resources/game_list.dart:
msgid "Remember the sequence you hear"
msgstr "Souvenez-vous de la séquence que vous entendez"

#: lib/resources/game_list.dart:
msgid "Which sequence was it?"
msgstr "Quelle séquence était-ce ?"

#: lib/resources/game_list.dart:
msgid "Listen to the sequence of sounds and remember it"
msgstr "Écoutez la séquence sonore et souvenez-vous-en"

#: lib/resources/game_list.dart:
msgid "Count the animals"
msgstr "Comptez les animaux"

#: lib/resources/game_list.dart:
msgid "Count the animals you see"
msgstr "Compte les animaux que vous voyez"

#: lib/resources/game_list.dart:
msgid "Count the animals you see on the screen and answer the question"
msgstr "Comptez les animaux que vous voyez à l'écran et répondez à la question"

#: lib/resources/game_list.dart:
msgid "Count the lights"
msgstr "Comptez les lumières"

#: lib/resources/game_list.dart:
msgid "Count how many lights come on"
msgstr "Comptez combien de fois les lumières s'allument"

#: lib/resources/game_list.dart:
msgid "How many light have come on?"
msgstr "Combien de fois les lumières se sont allumées ?"

#: lib/resources/game_list.dart:
msgid "Some lights are about to come on in rapid succession.\n\nCount how many lights come on."
msgstr "Des lumières sont sur le point de s'allumer en succession rapide.\n\nComptez combien de lumières s'allument."

#: lib/resources/game_list.dart:
msgid "Countdown"
msgstr "Compte à rebours"

#: lib/resources/game_list.dart:
msgid "Count the seconds in your head"
msgstr "Compyez les secondes dans votre tête"

#: lib/resources/game_list.dart:
msgid "Count the seconds in your head and tap when you think the countdown is over"
msgstr "Comptez les secondes dans votre tête et appuyez quand vous pensez que le compte à rebours est fini"

#: lib/resources/game_list.dart:
msgid "Elapsed time"
msgstr "Temps écoulé"

#: lib/resources/game_list.dart:
msgid "Calculate how much time has passed"
msgstr "Calculez combien de temps s'est écoulé"

#: lib/resources/game_list.dart:
msgid "How much time has passed?"
msgstr "Combien de temps s'est écoulé ?"

#: lib/resources/game_list.dart:
msgid "Calculate how much time has passed between two times"
msgstr "Calculez combien de temps s'est écoulé entre deux moments"

#: lib/resources/game_list.dart:
msgid "Fast finger"
msgstr "Doigt rapide"

#: lib/resources/game_list.dart:
msgid "Fastest object"
msgstr "Objet le plus rapide"

#: lib/resources/game_list.dart:
msgid "Find the fastest object"
msgstr "Trouvez l'objet le plus rapide"

#: lib/resources/game_list.dart:
msgid "Which one is the fastest object?"
msgstr "Quel objet est le plus rapide ?"

#: lib/resources/game_list.dart:
msgid "Try to figure out which one is the fastest moving object"
msgstr "Essayez de trouver quel objet est le plus rapide"

#: lib/resources/game_list.dart:
msgid "Find the cat"
msgstr "Trouvez le chat"

#: lib/resources/game_list.dart:
msgid "Tap when you see a cat"
msgstr "Appuyez quand vous voyez un chat"

#: lib/resources/game_list.dart:
msgid "Is there a cat?"
msgstr "Y a-t-il un chat ?"

#: lib/resources/game_list.dart:
msgid "Tap the screen when you see a cat"
msgstr "Appuyez sur l'écran quand vous voyez un chat"

#: lib/resources/game_list.dart:
msgid "Five objects"
msgstr "Cinq objets"

#: lib/resources/game_list.dart:
msgid "Tap when there are 5 different objects"
msgstr "Appuyez quand vous voyez 5 objets différents"

#: lib/resources/game_list.dart:
msgid "Are there 5 different objects?"
msgstr "Y a-t-il 5 objets différents ?"

#: lib/resources/game_list.dart:
msgid "Tap when there are at least 5 different objects"
msgstr "Appuyez quand il y a au moins 5 objets différents"

#: lib/resources/game_list.dart:
msgid "Hear and calculate"
msgstr "Entendez et calculez"

#: lib/resources/game_list.dart:
msgid "Add or subtract based on vibrations"
msgstr "Additionnez ou soustrayez selon les vibrations"

#: lib/resources/game_list.dart:
msgid "What is the result?"
msgstr "Quel est le résultat ?"

#: lib/resources/game_list.dart:
msgid "Sum if you hear a long vibration, subtract if you hear a short one.\n\nThe amount of the sum and the subtraction will be shown during the game.\n\n__ is the representation of a long vibration\n-  is the representation of a short vibration"
msgstr "Additionnez si vous entendez une vibration longue, soustrayez si vous en entendez une courte.\n\nLe montant de la somme et de la soustraction sera affiché pendant le jeu.\n\n__ est la représentation d'une vibration longue\n- est la représentation d'une vibration courte"

#: lib/resources/game_list.dart:
msgid "Lights on"
msgstr "Lumières allumées"

#: lib/resources/game_list.dart:
msgid "Count if there are more lights on than off"
msgstr "Comptez s'il y a plus de lumières allumées qu'éteintes"

#: lib/resources/game_list.dart:
msgid "Are there more lights on than off?"
msgstr "Y a-t-il plus de lumières allumées qu'éteintes ?"

#: lib/resources/game_list.dart:
msgid "Tap the screen if there are more yellow squares than black ones"
msgstr "Touchez l'écran s'il y a plus de carrés jaunes que de carrés noirs"

#: lib/resources/game_list.dart:
msgid "Listen and calculate"
msgstr "Écoutez et calculez"

#: lib/resources/game_list.dart:
msgid "Add or subtract based on what you hear"
msgstr "Additionnez ou soustrayez selon ce que vous entendez"

#: lib/resources/game_list.dart:
msgid "Sum if you hear a duck, subtract if you hear a cat.\n\nThe amount of the sum and the subtraction will be shown during the game."
msgstr "Additionnez si vous entendez un canard, soustrayez si vous entendez un chat.\n\nLe montant de la somme et de la soustraction sera affiché pendant le jeu."

#: lib/resources/game_list.dart:
msgid "Math calculation"
msgstr "Calcul mathématique"

#: lib/resources/game_list.dart:
msgid "Solve the equations"
msgstr "Résolvez les équations"

#: lib/resources/game_list.dart:
msgid "Solve the equations in your head as fast as you can"
msgstr "Résolvez les équations de tête le plus vite possible"

#: lib/resources/game_list.dart:
msgid "Memory"
msgstr "Mémoire"

#: lib/resources/game_list.dart:
msgid "Remember the objects and their position"
msgstr "Souvenez-vous des objets et de leur position"

#: lib/resources/game_list.dart:
msgid "What was in this position?"
msgstr "Qu'y avait-il à cette place ?"

#: lib/resources/game_list.dart:
msgid "Mix colors"
msgstr "Mélanger les couleurs"

#: lib/resources/game_list.dart:
msgid "Mix the colors"
msgstr "Mélangez les couleurs"

#: lib/resources/game_list.dart:
msgid "Try to figure out which color results from the color mixing"
msgstr "Essayez de comprendre quelle couleur on obtient en mélangeant d'autres couleurs"

#: lib/resources/game_list.dart:
msgid "Recognize the sound"
msgstr "Reconnaissez le son"

#: lib/resources/game_list.dart:
msgid "Tap the screen when you hear the sound"
msgstr "Touchez l'écran quand vous entendez le son"

#: lib/resources/game_list.dart:
msgid "Listen to the sample sound twice here.\n\nThen tap the screen when you hear it during a sequence of sounds."
msgstr "Écoutez le son de l'échantillon deux fois ici.\n\nAppuyez ensuite sur l'écran quand vous l'entendez pendant une séquence sonore."

#: lib/resources/game_list.dart:
msgid "Reflexes"
msgstr "Réflexes"

#: lib/resources/game_list.dart:
msgid "Tap at the right moment"
msgstr "Touchez au bon moment"

#: lib/resources/game_list.dart:
msgid "Tap as soon as the screen becomes colored"
msgstr "Appuyez dès que l'écran devient coloré"

#: lib/resources/game_list.dart:
msgid "As soon as the screen becomes colored, tap the option of the same color.\n\nKeep in mind that you have little time before the screen turns white again."
msgstr "Dès que l'écran devient coloré, touchez l'option de la même couleur.\n\nGardez à l'esprit que vous avez peu de temps avant que l'écran redevienne blanc."

#: lib/resources/game_list.dart:
msgid "Remember"
msgstr "Souvenez-vous"

#: lib/resources/game_list.dart:
msgid "Remember the colors and letters"
msgstr "Souvenez-vous des couleurs et des lettres"

#: lib/resources/game_list.dart:
msgid "Remember the sequence of colors and letters that will appear on the screen"
msgstr "Souvenez-vous de la séquence de couleurs et de lettres qui apparaîtra à l'écran"

#: lib/resources/game_list.dart:
msgid "Remember the animals"
msgstr "Souvenez-vous des animaux"

#: lib/resources/game_list.dart:
msgid "Remember the animals you hear and see"
msgstr "Souvenez-vous des animaux que vous entendez et voyez"

#: lib/resources/game_list.dart:
msgid "Remember the sequences of animals that you will hear, see and read.\n\nKeep in mind that what you see is not what you hear."
msgstr "Souvenez-vous des séquences d'animaux que vous allez entendre, voir et lire.\n\nGardez à l'esprit que ce que vous voyez n'est pas ce que vous entendez."

#: lib/resources/game_list.dart:
msgid "Shortest vibration"
msgstr "Vibration la plus courte"

#: lib/resources/game_list.dart:
msgid "Find the shortest vibration"
msgstr "Trouvez la vibration la plus courte"

#: lib/resources/game_list.dart:
msgid "Which one was the shortest vibration?"
msgstr "Quelle était la vibration la plus courte ?"

#: lib/resources/game_list.dart:
msgid "Try to figure out which one is the shortest vibration among four"
msgstr "Trouvez quelle vibration est la plus courte parmi les quatre"

#: lib/resources/game_list.dart:
msgid "Sound repetitions"
msgstr "Répétitions de sons"

#: lib/resources/game_list.dart:
msgid "Count how many times you hear the sound"
msgstr "Comptez combien de fois vous entendez le son"

#: lib/resources/game_list.dart:
msgid "How many times have you heard the sound?"
msgstr "Combien de fois avez-vous entendu le son ?"

#: lib/resources/game_list.dart:
msgid "Three figures"
msgstr "Trois figures"

#: lib/resources/game_list.dart:
msgid "Find three identical figures"
msgstr "Trouvez trois figures identiques"

#: lib/resources/game_list.dart:
msgid "Are there 3 identical figures?"
msgstr "Y a-t-il 3 formes identiques ?"

#: lib/resources/game_list.dart:
msgid "Tap the screen when you see at least 3 figures that have the same color and the same letter inside"
msgstr "Touchez l'écran quand vous voyez au moins 3 figures qui ont la même couleur et la même lettre à l'intérieur"

#: lib/resources/game_list.dart:
msgid "Vibration repetitions"
msgstr "Répétitions de vibration"

#: lib/resources/game_list.dart:
msgid "Count how many times you hear the vibration"
msgstr "Comptez combien de fois vous entendez la vibration"

#: lib/resources/game_list.dart:
msgid "How many times have you heard the vibration?"
msgstr "Combien de fois avez-vous entendu la vibration ?"

#: lib/resources/game_list.dart:
msgid "Vibration sequence"
msgstr "Séquence de vibrations"

#: lib/resources/game_list.dart:
msgid "Listen to the sequence of vibrations and remember it.\n\n__ is the representation of a long vibration\n-  is the representation of a short vibration"
msgstr "Écoutez la séquence de vibrations et souvenez-vous-en.\n\n__ est la représentation d'une vibration longue\n-  est la représentation d'une vibration courte"

#: lib/resources/game_list.dart:
msgid "What color is it"
msgstr "De quelle couleur est-ce"

#: lib/resources/game_list.dart:
msgid "Recognize which color is used"
msgstr "Reconnaissez la couleur utilisée"

#: lib/resources/game_list.dart:
msgid "Answer as fast as you can which color is written or which color is used for the background or for the text"
msgstr "Répondez aussi vite que possible quelle couleur est écrite ou quelle couleur est utilisée pour l'arrière-plan ou pour le texte"

#: lib/resources/game_category.dart:
msgid "Logic"
msgstr "Logique"

#: lib/resources/game_category.dart:
msgid "Sound"
msgstr "Sonore"

#: lib/resources/game_category.dart:
msgid "Vibration"
msgstr "Vibration"
