* 10 nieuwe spellen
* Kleine verbeteringen voor de gebruikerservaring
* Nieuwe optie om vibratie-spellen uit te schakelen
* Bijwerken vertalingen
* Oplossen van bugs
