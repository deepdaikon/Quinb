import 'dart:async';
import 'dart:math';

import 'package:quinb/game/audio.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/local_data_controller.dart';
import 'package:vibration/vibration.dart';

/// Count repetitions
class CountGame extends Game with Audio {
  CountGame(int players, GamePageState gamePage, {this.useVibration = false})
      : super(players, gamePage);

  /// True if outputs are vibrations instead of sounds
  final bool useVibration;

  /// Sound played
  final String beep = 'sound_1.wav';

  @override
  Future<void> prePhase() async {
    int lowestValue = Random().nextInt(8) + max(1, settings.difficulty.index);
    options = [for (var i = 0; i < 5; i++) lowestValue + i].toOptions()
      ..shuffle();
    correctValue = options[Random().nextInt(options.length)].value;
    for (var i = 0; i < correctValue!; i++) {
      if (!isPrePhase) break;
      await Future.delayed(Duration(milliseconds: i == 0 ? 1000 : 200));
      await (useVibration ? Vibration.vibrate(duration: 150) : playSound(beep));
    }
  }
}
