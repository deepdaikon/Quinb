import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quinb/game/audio.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/colors.dart';
import 'package:quinb/game/utils/emoji.dart';
import 'package:quinb/game/utils/extensions.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Remember the sequences (color+letter or sound+emoji+string)
class RememberGame extends Game with Audio {
  RememberGame(int players, GamePageState gamePage, {this.soundMode = false})
      : super(players, gamePage);
  bool soundMode;

  @override
  String get question {
    if (soundMode) {
      return (element == Element.emoji
              ? 'What animal was the %s one you saw?'.i18n
              : (element == Element.sound
                  ? 'What animal was the %s one you heard?'.i18n
                  : 'What animal was the %s one you read?'.i18n))
          .fill([ordinal[correctValue!]]);
    } else {
      return (element == Element.color
              ? 'What color was the %s figure?'.i18n
              : 'What letter was in the %s figure?'.i18n)
          .fill([ordinal[correctValue!]]);
    }
  }

  /// Ordinal numbers
  final List<String> ordinal = [
    '1st'.i18n,
    '2nd'.i18n,
    '3rd'.i18n,
    '4th'.i18n,
    '5th'.i18n,
    '6th'.i18n,
  ];
  final List<String> letterList = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');

  /// Text
  String? currentText;
  List<String>? textSequence;

  /// Colors
  Color? currentColor;
  List<Color>? colorSequence;

  /// Emoji
  Animal? currentAnimal;
  List<Animal>? animalSequence;

  /// Sounds
  Animal? currentSound;
  List<Animal>? soundSequence;

  /// String, color, emoji or sound
  late Element element;

  @override
  Future<void> prePhase() async {
    if (soundMode) {
      element =
          [Element.text, Element.emoji, Element.sound][Random().nextInt(3)];
      animalSequence = Animal.values.toList();
      soundSequence = Animal.values.toList();
      textSequence = Animal.values.map((e) => e.name.i18n).toList();
      animalSequence!.shuffle();
      soundSequence!.shuffle();
      textSequence!.shuffle();
      List tempList;
      if (element == Element.text) {
        tempList = List.from(textSequence!);
      } else if (element == Element.sound) {
        tempList = List.from(soundSequence!);
      } else {
        tempList = List.from(animalSequence!);
      }
      options = [
        for (var t in tempList)
          Option(tempList.indexOf(t),
              text: (((t is Animal) ? t.name.i18n : t) as String).capitalize)
      ]..shuffle();
      correctValue = options[Random().nextInt(options.length)].value;

      for (var i = 0; i < soundSequence!.length; i++) {
        if (!isPrePhase) return;
        currentText = textSequence![i];
        currentSound = soundSequence![i];
        currentAnimal = animalSequence![i];
        playSound(currentSound!.fileName);
        ui.refresh();
        await Future.delayed(
            Duration(milliseconds: 1300 + Game.invertedDifficultyIndex * 100));
      }
    } else {
      element = Random().nextBool() ? Element.color : Element.text;
      textSequence = letterList
          .getRandomSublist(settings.difficulty.index + 2)
          .cast<String>()
          .toList();
      colorSequence = colorList
          .getRandomSublist(settings.difficulty.index + 2)
          .cast<Color>()
          .toList();
      textSequence!.shuffle();
      colorSequence!.shuffle();
      var tempList =
          List.from(element == Element.color ? colorSequence! : textSequence!);
      options = [
        for (var t in tempList)
          Option(tempList.indexOf(t),
              text: element == Element.color ? (t as Color).name : t)
      ]..shuffle();
      correctValue = options[Random().nextInt(options.length)].value;

      for (var i = 0; i < textSequence!.length; i++) {
        if (!isPrePhase) return;
        currentColor = colorSequence![i];
        currentText = textSequence![i];
        ui.refresh();
        await Future.delayed(const Duration(seconds: 1));
      }
    }
  }

  @override
  Widget widget() => isPrePhase
      ? Center(
          child: Container(
            height: widgetHeight / 1.5,
            width: widgetHeight / 1.5,
            color: soundMode ? null : currentColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                if (multiplayer) innerWidget(rotated: true),
                if (soundMode)
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 6),
                    child: RotatedBox(
                      quarterTurns: multiplayer ? Random().nextInt(3) : 0,
                      child: Center(
                        child: Text(
                          currentAnimal!.emoji,
                          style: TextStyle(
                              fontFamily: 'emoji', fontSize: widgetHeight / 3),
                        ),
                      ),
                    ),
                  ),
                innerWidget(),
              ],
            ),
          ),
        )
      : super.widget();

  Widget innerWidget({bool rotated = false}) => Expanded(
        child: RotatedBox(
          quarterTurns: rotated ? 2 : 0,
          child: FittedBox(
            fit: BoxFit.fitHeight,
            child: Text(
              soundMode ? currentText!.i18n : currentText!,
              style: TextStyle(color: soundMode ? Colors.black : Colors.white),
            ),
          ),
        ),
      );
}

enum Element { text, color, emoji, sound }
