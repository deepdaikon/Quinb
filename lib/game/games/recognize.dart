import 'dart:async';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:quinb/game/audio.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Recognize a sound or sequence of sounds
class RecognizeGame extends Game with Audio {
  RecognizeGame(int players, GamePageState gamePage) : super(players, gamePage);

  @override
  String get tip => samplesToBePlayed > 0
      ? 'Tap here to listen to the sample'.i18n +
          '\n($samplesToBePlayed / $maxSamples)'
      : 'Tap here to start'.i18n;

  @override
  String get question => sequenceLength > 1
      ? 'Tap when you hear the previous sequence'.i18n
      : 'Tap when you hear the previous sound'.i18n;

  @override
  int time = 1500;

  @override
  bool autoStart = false;

  @override
  List<Option> options = [Option.tap];

  /// Length of the sequence to be recognized
  final int sequenceLength = max(1, settings.difficulty.index - 1);

  /// Times the player can play the sample
  static const int maxSamples = 2;

  /// Number of sample yet to be played before playing
  int samplesToBePlayed = maxSamples;

  /// Sounds to be recognized by players
  late List<String> sequenceToBeRecognized;

  /// Playing sequence
  late List<String> sequence;

  /// Index in sequence of the sound being played
  late int currentlyPlayingIndex;

  /// True if it is playing a sample sound
  bool playingSample = false;

  /// Function executed when tapping on the main widget if isPrePhase
  void playSample() async {
    if (playingSample) return;
    if (samplesToBePlayed == maxSamples) {
      sequence = List.generate(
          16, (_) => Audio.soundList[Random().nextInt(Audio.soundList.length)]);
      final int start = Random().nextInt(16 - sequenceLength);
      sequenceToBeRecognized = sequence.sublist(start, start + sequenceLength);
    }
    if (--samplesToBePlayed >= 0) {
      playingSample = true;
      for (var index = 0; index < sequenceLength; index++) {
        await playSound(sequenceToBeRecognized[index]);
        await Future.delayed(const Duration(milliseconds: 800));
      }
      ui.refresh();
      playingSample = false;
    } else {
      newTurn();
    }
  }

  @override
  Future<void> prePhase() async {
    samplesToBePlayed = maxSamples;
    currentlyPlayingIndex = -1;
  }

  @override
  void update() {
    if (++currentlyPlayingIndex >= sequence.length) currentlyPlayingIndex = 0;
    playSound(sequence[currentlyPlayingIndex]);
  }

  @override
  bool validateInput(int input) =>
      currentlyPlayingIndex >= sequenceLength - 1 &&
      listEquals(
          sequence.sublist(currentlyPlayingIndex - sequenceLength + 1,
              currentlyPlayingIndex + 1),
          sequenceToBeRecognized);

  @override
  Widget interface() => isPrePhase
      ? Expanded(
          child: InkWell(
            enableFeedback: false,
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            hoverColor: Colors.transparent,
            onTap: playSample,
            child: super.widget(),
          ),
        )
      : super.interface();
}
