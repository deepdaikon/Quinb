import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Counts the seconds in your head
class CountdownGame extends Game {
  CountdownGame(int players, GamePageState gamePage) : super(players, gamePage);

  @override
  int time = 100;

  @override
  bool hideTimer = true;

  final Stopwatch stopwatch = Stopwatch();
  late int seconds;
  int get remainingMilliseconds =>
      seconds * 1000 - stopwatch.elapsedMilliseconds;
  final isItOver = 'Is the countdown over?'.i18n;

  @override
  String get question {
    if (stopwatch.elapsed.inSeconds < 3) {
      var duration = Duration(milliseconds: remainingMilliseconds);
      String seconds = duration.inSeconds.remainder(60).toString();
      String milliseconds =
          duration.inMilliseconds.remainder(1000).toString()[0];
      return '$seconds.$milliseconds';
    }
    if (options.isEmpty) options = [Option.yes];
    return isItOver;
  }

  @override
  Future<void> prePhase() async {
    options = [];
    seconds = 4 + settings.difficulty.index + Random().nextInt(4);
    stopwatch.reset();
    stopwatch.start();
  }

  @override
  void pause([pauseMenu]) {
    super.pause();
    isPaused ? stopwatch.stop() : stopwatch.start();
  }

  @override
  bool validateInput(int input) {
    stopwatch.stop();
    return remainingMilliseconds <= 0;
  }

  @override
  Widget widget() => isPostPhase
      ? Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            if (multiplayer) ...[
              RotatedBox(
                  quarterTurns: 2,
                  child: innerWidget([1, 3].contains(lastInput!.player))),
              const Divider(height: 1),
            ],
            innerWidget([0, 2].contains(lastInput!.player))
          ],
        )
      : super.widget();

  // ignore: avoid_positional_boolean_parameters
  Widget innerWidget(bool responded) {
    var text = '';
    if (remainingMilliseconds > 0) {
      text = responded
          ? 'Too soon!'.i18n + ' (- $remainingMilliseconds ms)'
          : '- $remainingMilliseconds ms';
    } else {
      text = responded
          ? 'Good!'.i18n + ' (+ ${remainingMilliseconds.abs()} ms)'
          : '+ ${remainingMilliseconds.abs()} ms';
    }
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: FittedBox(
          fit: BoxFit.fitWidth,
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.grey[800], fontSize: 28),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    stopwatch.stop();
    super.dispose();
  }
}
