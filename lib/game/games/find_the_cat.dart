import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/emoji.dart';
import 'package:quinb/game/utils/object.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Find the cat
class FindTheCatGame extends Game {
  FindTheCatGame(int players, GamePageState gamePage)
      : itemCount = settings.difficulty.index <= 1
            ? 6
            : (settings.difficulty.index <= 3 ? 8 : 10),
        super(players, gamePage);

  @override
  final int time = 800 + Game.invertedDifficultyIndex * 200;

  /// Number of emoji displayed
  final int itemCount;

  /// List of objects
  late List<GameObject> objectList;

  /// List of emoji available (without cat)
  final List<String> emojiSublist =
      emojis.where((e) => e != Animal.cat.emoji).toList();

  @override
  void update() {
    objectList = List<GameObject>.generate(
        itemCount,
        (_) => GameObject(
            text: emojiSublist[Random().nextInt(emojiSublist.length)]));
    if (Random().nextDouble() > 0.65) {
      objectList[Random().nextInt(objectList.length)] =
          GameObject(text: Animal.cat.emoji);
      correctValue = Option.tap.value;
    } else {
      correctValue = 0;
    }
  }

  @override
  Widget widget() => questionWidget(
        child: GridView.count(
          physics: const NeverScrollableScrollPhysics(),
          crossAxisCount: itemCount ~/ 2,
          childAspectRatio:
              (MediaQuery.of(ui.context).size.width / (itemCount ~/ 2)) /
                  (innerWidgetHeight / 2),
          children: List.generate(itemCount, (index) {
            return Container(
              margin: const EdgeInsets.all(4),
              decoration: isPostPhase &&
                      correctValue != 0 &&
                      objectList[index].text == Animal.cat.emoji
                  ? BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.green, width: 5),
                    )
                  : const BoxDecoration(),
              child: Center(
                child: RotatedBox(
                  quarterTurns: objectList[index].quarterTurns,
                  child: Text(
                    objectList[index].text,
                    style: const TextStyle(fontSize: 30, fontFamily: 'emoji'),
                  ),
                ),
              ),
            );
          }),
        ),
      );
}
