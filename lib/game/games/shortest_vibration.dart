import 'dart:async';

import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';
import 'package:vibration/vibration.dart';

/// Find the shortest vibration
class ShortestVibrationGame extends Game {
  ShortestVibrationGame(int players, GamePageState gamePage)
      : super(players, gamePage);

  @override
  List<Option> options = [
    Option(0, text: 'First'.i18n),
    Option(1, text: 'Second'.i18n),
    Option(2, text: 'Third'.i18n),
    Option(3, text: 'Fourth'.i18n),
  ];

  /// Durations of vibrations
  final List<int> durations = List.generate(
      4, (index) => 125 + index * (15 + (4 - settings.difficulty.index) * 5));

  @override
  Future<void> prePhase() async {
    durations.shuffle();
    for (var i = 0; i < options.length; i++) {
      await Future.delayed(Duration(milliseconds: i == 0 ? 1000 : 500));
      if (isPrePhase) await Vibration.vibrate(duration: durations[i]);
    }
    correctValue = durations.indexOf((List.from(durations)..sort()).first);
  }
}
