import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Tap when the list of numbers is in order
class AscendingOrderGame extends Game {
  AscendingOrderGame(int players, GamePageState gamePage)
      : super(players, gamePage);

  @override
  final int time = 2000 + Game.invertedDifficultyIndex * 200;

  late List<int> numbers;
  late int length;
  final List<int> wrongNumbers = [];

  @override
  void update() {
    wrongNumbers.clear();
    length = 5 + settings.difficulty.index + Random().nextInt(3);
    numbers = [for (var i = 1; i <= length + 3; i++) i];
    for (var i = 0; i < 3; i++) {
      numbers.removeAt(Random().nextInt(numbers.length));
    }
    numbers.sort((a, b) {
      if (Random().nextDouble() > 0.9) {
        wrongNumbers.addAll([a, b]);
        return b.compareTo(a);
      }
      return a.compareTo(b);
    });
    correctValue = wrongNumbers.isEmpty ? Option.tap.value : 0;
  }

  @override
  Widget widget() => questionWidget(
        child: Column(
          children: [
            if (multiplayer) ...[
              innerWidget(rotated: true),
              const Divider(height: 1),
            ],
            innerWidget(),
          ],
        ),
      );

  Widget innerWidget({bool rotated = false}) => Expanded(
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(ui.context).size.width / 9),
          child: FittedBox(
            fit: BoxFit.fitWidth,
            child: RotatedBox(
              quarterTurns: rotated ? 2 : 0,
              child: RichText(
                text: TextSpan(
                  style: const TextStyle(letterSpacing: 5, color: Colors.black),
                  children: <TextSpan>[
                    for (var n in numbers) ...[
                      TextSpan(
                        text: n.toString(),
                        style: isPostPhase
                            ? TextStyle(
                                color: wrongNumbers.isEmpty
                                    ? Colors.green
                                    : (wrongNumbers.contains(n)
                                        ? Colors.red
                                        : Colors.black),
                              )
                            : null,
                      ),
                      if (numbers.last != n) const TextSpan(text: ','),
                    ]
                  ],
                ),
              ),
            ),
          ),
        ),
      );
}
