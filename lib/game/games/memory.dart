import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/emoji.dart';
import 'package:quinb/game/utils/extensions.dart';
import 'package:quinb/game/utils/object.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Remember the objects and their position
class MemoryGame extends Game {
  MemoryGame(int players, GamePageState gamePage)
      : itemCount = settings.difficulty.index <= 1
            ? 6
            : (settings.difficulty.index <= 3 ? 8 : 10),
        super(players, gamePage);

  @override
  int? correctValue = 4;

  /// Number of emoji displayed
  final int itemCount;
  static const int optionCount = 4;

  /// List of objects
  late List<GameObject> objectList;
  late int position;

  @override
  Future<void> prePhase() async {
    objectList = emojis
        .getRandomSublist(itemCount)
        .map((e) => GameObject(text: e))
        .toList();
    position = Random().nextInt(itemCount);
    List<GameObject> optionSubList = List.from(objectList);
    optionSubList.removeAt(position);
    optionSubList = optionSubList
        .getRandomSublist(optionCount - 1)
        .toList()
        .cast<GameObject>();
    options = [
      Option(correctValue!, text: objectList[position].text),
      for (var i = 0; i < optionSubList.length; i++)
        Option(i, text: optionSubList[i].text)
    ]..shuffle();
    await Future.delayed(
        Duration(milliseconds: 4000 + Game.invertedDifficultyIndex * 200));
  }

  @override
  Widget widget() => questionWidget(
        text: isPrePhase ? tip : question!,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 4),
          child: GridView.count(
            physics: const NeverScrollableScrollPhysics(),
            crossAxisCount: itemCount ~/ 2,
            childAspectRatio:
                (MediaQuery.of(ui.context).size.width / (itemCount ~/ 2)) /
                    (innerWidgetHeight / 2),
            children: List.generate(itemCount, (index) {
              return Container(
                margin: const EdgeInsets.all(4),
                decoration: !isPrePhase && index == position
                    ? BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(
                            color: lastInput != null
                                ? (lastInput!.correct!
                                    ? Colors.green
                                    : Colors.red)
                                : Colors.blue,
                            width: 5))
                    : const BoxDecoration(),
                child: Center(
                  child: RotatedBox(
                    quarterTurns:
                        multiplayer ? objectList[index].quarterTurns : 0,
                    child: Text(
                      isMainPhase ? '?' : objectList[index].text,
                      style: const TextStyle(fontSize: 30, fontFamily: 'emoji'),
                    ),
                  ),
                ),
              );
            }),
          ),
        ),
      );
}
