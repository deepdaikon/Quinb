import 'dart:async';
import 'dart:math';

import 'package:quinb/game/audio.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/emoji.dart';
import 'package:quinb/game/utils/extensions.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/i18n.dart';

/// Animal farm game
class AnimalFarmGame extends Game with Audio {
  AnimalFarmGame(int players, GamePageState gamePage)
      : super(players, gamePage);

  @override
  String get question =>
      'Tap when you hear a %s'.i18n.fill([correctAnimal.name.i18n]);

  @override
  final int time = 800 + Game.invertedDifficultyIndex * 100;

  @override
  List<Option> options = [Option.tap];

  /// Sequence of sounds
  late List<String> sequence;

  /// Animal to find
  late Animal correctAnimal;

  /// Index in sequence of the sound being played
  late int currentlyPlayingIndex;

  @override
  Future<void> prePhase() async {
    correctAnimal = Animal.values[Random().nextInt(Animal.values.length)];
    sequence = List.from(Audio.soundList)
      ..retainWhere(
          (s) => ![correctAnimal.name, 'sound'].contains(s.split('_').first))
      ..getRandomSublist(8);
    currentlyPlayingIndex = -1;
    sequence[1 + Random().nextInt(6)] = correctAnimal.fileName;
  }

  @override
  void update() {
    if (++currentlyPlayingIndex >= sequence.length) currentlyPlayingIndex = 0;
    playSound(sequence[currentlyPlayingIndex]);
  }

  @override
  bool validateInput(int input) =>
      currentlyPlayingIndex >= 0 &&
      sequence[currentlyPlayingIndex] == correctAnimal.fileName;
}
