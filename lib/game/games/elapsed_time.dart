import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Calculate how much time has passed
class ElapsedTimeGame extends Game {
  ElapsedTimeGame(int players, GamePageState gamePage)
      : super(players, gamePage);

  @override
  final int time = 5000 + Game.invertedDifficultyIndex * 200;

  late DateTime start;
  String dateToString(DateTime date) => settings.use24h
      ? date.hour.toString().padLeft(2, '0') +
          ':' +
          date.minute.toString().padLeft(2, '0')
      : ((date.hour + 11) % 12 + 1).toString().padLeft(2, '0') +
          ':' +
          date.minute.toString().padLeft(2, '0') +
          ' ' +
          (date.hour < 12 ? 'AM' : 'PM');

  String elapsed(int minutes) {
    List<String> parts = Duration(minutes: minutes).toString().split(':');
    return '${parts[0].padLeft(2, '0')}:${parts[1].padLeft(2, '0')}';
  }

  @override
  void update() {
    correctValue = 5 + settings.difficulty.index * 30 + Random().nextInt(200);
    start = DateTime(2000, 1, 1, Random().nextInt(24), Random().nextInt(60));
    options = [Option(correctValue!, text: elapsed(correctValue!))];
    for (final i in <int>[5, 10, 15]) {
      var value = correctValue! +
          (correctValue! - i >= 0 && Random().nextBool() ? -i : i);
      options.add(Option(value, text: elapsed(value)));
    }
    options.shuffle();
  }

  @override
  Widget widget() => questionWidget(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            if (multiplayer) ...[
              RotatedBox(quarterTurns: 2, child: innerWidget()),
              const Divider(height: 1),
            ],
            innerWidget()
          ],
        ),
      );

  Widget innerWidget() => FittedBox(
        fit: BoxFit.fitWidth,
        child: Text(
          '${dateToString(start)} → '
          '${dateToString(start.add(Duration(minutes: correctValue!)))}',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 48, color: Colors.grey[800], fontFamily: 'dd5x7'),
        ),
      );
}
