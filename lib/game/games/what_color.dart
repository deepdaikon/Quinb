import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/colors.dart';
import 'package:quinb/game/utils/extensions.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/i18n.dart';

/// What color is it
class WhatColorGame extends Game {
  WhatColorGame(int players, GamePageState gamePage) : super(players, gamePage);

  @override
  String get question {
    switch (element) {
      case Element.textColor:
        return 'What color is the text?'.i18n;
      case Element.background:
        return 'What color is the background?'.i18n;
      case Element.textString:
        return 'What color is written?'.i18n;
    }
  }

  @override
  final int time = 2000 + Game.invertedDifficultyIndex * 200;

  /// Asking for background, text or text color
  late Element element;

  /// Colors used
  late Color textString;
  late Color textColor;

  late List<Color> colors;

  @override
  void update() {
    colors = colorList.getRandomSublist(4).cast<Color>().toList();
    options.clear();
    for (var i = 0; i < colors.length; i++) {
      options.add(Option(colors[i].value, text: colors[i].name));
    }
    options.shuffle();
    backgroundColor = colors[Random().nextInt(colors.length)];
    colors.remove(backgroundColor);

    textString = colors[Random().nextInt(colors.length)];
    colors.remove(textString);

    textColor = colors[Random().nextInt(colors.length)];
    colors.remove(textColor);

    element = Element.values[Random().nextInt(Element.values.length)];
    switch (element) {
      case Element.background:
        correctValue = backgroundColor.value;
        break;
      case Element.textColor:
        correctValue = textColor.value;
        break;
      case Element.textString:
        correctValue = textString.value;
        break;
    }
  }

  @override
  Widget widget() => questionWidget(
        textColor: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            if (multiplayer) RotatedBox(quarterTurns: 2, child: innerWidget()),
            innerWidget(),
          ],
        ),
      );

  Widget innerWidget() {
    return FittedBox(
      fit: BoxFit.fitWidth,
      child: Stack(
        children: <Widget>[
          Text(
            textString.name.toUpperCase(),
            style: TextStyle(
              fontSize: 36,
              letterSpacing: 6,
              fontWeight: FontWeight.w700,
              foreground: Paint()
                ..style = PaintingStyle.stroke
                ..strokeWidth = 3
                ..color = Colors.white,
            ),
          ),
          Text(
            textString.name.toUpperCase(),
            style: TextStyle(
              fontSize: 36,
              letterSpacing: 6,
              color: textColor,
              fontWeight: FontWeight.w700,
            ),
          ),
        ],
      ),
    );
  }
}

enum Element { background, textString, textColor }
