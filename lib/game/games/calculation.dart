import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/game/utils/extensions.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Math calculation game and Color calculation game
class CalculationGame extends Game {
  CalculationGame(int players, GamePageState gamePage, {this.colorMode = false})
      : maxValue = colorMode
            ? 0x000000FF - settings.difficulty.index * 0x00000022
            : 10 + settings.difficulty.index * 5,
        super(players, gamePage);

  @override
  String get question => '$firstValue ${operation.symbol} $secondValue = ?';

  @override
  final int time = 2600 + Game.invertedDifficultyIndex * 200;

  /// Math or Color game
  final bool colorMode;

  /// Operands
  late int firstValue;
  late int secondValue;

  /// Maximum operand value
  late int maxValue;

  /// Mathematical operation
  Operation operation = Operation.addition;

  @override
  void update() {
    if (colorMode) {
      firstValue = Random().nextColorValue(0x000000FF);
      secondValue = Random().nextColorValue(0x000000FF);
      correctValue = (Color(firstValue) + Color(secondValue)).value;
      options = <int>[
        correctValue!,
        for (var i = 0; i < min(4, settings.difficulty.index + 1); i += 1)
          (Random().nextBool()
                  ? Color(firstValue) + Color(Random().nextColorValue(maxValue))
                  : Color(firstValue) -
                      Color(Random().nextColorValue(maxValue)))
              .value
      ].toOptions(asColors: true)
        ..shuffle();
    } else {
      operation = Operation.values[Random().nextInt(
          settings.difficulty.index < 2 ? 2 : Operation.values.length)];
      secondValue = 1 + Random().nextInt(maxValue);
      switch (operation) {
        case Operation.addition:
          firstValue = Random().nextInt(maxValue);
          correctValue = firstValue + secondValue;
          break;
        case Operation.subtraction:
          firstValue = secondValue + Random().nextInt(maxValue);
          correctValue = firstValue - secondValue;
          break;
        case Operation.multiplication:
          firstValue = Random().nextInt(maxValue);
          correctValue = firstValue * secondValue;
          break;
        case Operation.division:
          correctValue = Random().nextInt(maxValue);
          firstValue = secondValue * correctValue!;
          break;
      }
      options = [
        correctValue!,
        for (var i in <int>[3, 5, 7, 10])
          correctValue! +
              (correctValue! >= i && Random().nextBool() ? -1 : 1) * i
      ].toOptions()
        ..shuffle();
    }
  }

  @override
  Widget widget() => colorMode
      ? Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            CircleAvatar(
                radius: widgetHeight / 5, backgroundColor: Color(firstValue)),
            Text(operation.symbol, style: const TextStyle(fontSize: 30)),
            CircleAvatar(
                radius: widgetHeight / 5, backgroundColor: Color(secondValue)),
          ],
        )
      : super.widget();
}

enum Operation {
  addition('+'),
  subtraction('−'),
  multiplication('×'),
  division('÷');

  const Operation(this.symbol);
  final String symbol;
}
