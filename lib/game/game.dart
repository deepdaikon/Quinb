import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:quinb/game/utils/colors.dart';
import 'package:quinb/game/utils/emoji.dart';
import 'package:quinb/game/utils/game_phase.dart';
import 'package:quinb/game/utils/option.dart';
import 'package:quinb/game/utils/player.dart';
import 'package:quinb/resources/game_list.dart';
import 'package:quinb/resources/game_loader.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/ui/screens/game/widgets/pause_menu.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Generic game
abstract class Game {
  Game(int players, this.ui) : multiplayer = players > 1 {
    playerList = List<Player>.generate(
        players, (index) => Player(color: playerColor[index], id: index));
  }
  final bool multiplayer;

  /// Current GamePageState
  final GamePageState ui;

  /// Game ID and details
  late final GameId id;
  GameDetails get info => gameList.firstWhere((detail) => detail.id == id);

  /// Game phase
  GamePhase _phase = GamePhase.starting;
  bool get isStarting => _phase == GamePhase.starting;
  bool get isPrePhase => _phase == GamePhase.preMain;
  bool get isMainPhase => _phase == GamePhase.main;
  bool get isPostPhase => _phase == GamePhase.postMain;
  bool get isPaused => _phase == GamePhase.pause;
  bool get isOver => _phase == GamePhase.end;

  /// History of phase changes
  final List<GamePhase> _phaseLog = [];

  /// Round count
  int roundCount = 0;

  /// List of players in game
  late List<Player> playerList;

  /// List of players sorted by points
  List<Player> get ranking =>
      List.from(playerList)..sort((a, b) => b.points.compareTo(a.points));

  /// List of available options
  List<Option> options = [Option.yes];

  /// Value of the correct answer
  int? correctValue;

  /// Last input received by players
  Input? lastInput;

  /// Return: Difficulty.values.length (5) - difficulty index
  static final int invertedDifficultyIndex = 5 - settings.difficulty.index;

  /// Start automatically a new turn
  bool autoStart = true;

  /// Start a new game
  void startGame() {
    newPhase(GamePhase.preMain);
    if (autoStart) newTurn();
  }

  /// Start a new turn. It is called after init and after setInput()
  @mustCallSuper
  void newTurn() async {
    ++roundCount;
    elapsedTime = Duration.zero;
    await prePhase();
    if (isOver || isPaused) return;
    newPhase(GamePhase.main);
    await mainPhase();
  }

  /// Update _phase and _phaseLog
  void newPhase(GamePhase newPhase, {bool forced = false}) {
    if (newPhase == GamePhase.pause && isPaused) return;
    if (isPaused && !forced) {
      _phaseLog.addAll([newPhase, GamePhase.pause]);
    } else {
      _phase = newPhase;
      _phaseLog.add(_phase);
      ui.refresh();
    }
  }

  /// Elapsed time since new turn or since the last updater cycle
  /// Used by TimerWidget
  late Duration elapsedTime;

  /// Timer that game could use to update itself (objects, answers and questions)
  /// time: milliseconds between each update
  /// updater: called every cycle from ui (TimerWidget), should not be overridden
  int time = 0;
  VoidCallback? updater;
  bool hideTimer = false;
  void update() {}

  /// Game phases, the logic of the game must be placed here
  /// During a match, the game will go through all the phases each round
  /// Variables must be cleaned to avoid using values from the previous round
  Future<void> prePhase() async {}

  /// The default logic of mainPhase could be overridden
  Future<void> mainPhase() async {
    if (time == 0) return;
    update();
    updater = () {
      if (!isMainPhase) return;
      update();
      ui.refresh();
    };
  }

  /// Check if the answer is correct
  bool validateInput(int input) => input == correctValue;

  /// Insert input (by a player or timer)
  void setInput([Input? input]) {
    if (!isMainPhase) return;
    newPhase(GamePhase.postMain);
    if (input != null) {
      lastInput = input;
      lastInput!.correct = validateInput(input.option!.value);
    }
    ui.popupController(timeOver: input == null);
    ui.refresh();
    // Wait 3 seconds before saving input and changing game
    // If is not postPhase: _saveInput is made by pause()
    Future.delayed(const Duration(seconds: 3)).then((value) {
      if (ui.mounted && isPostPhase) _saveInput();
    });
  }

  /// Save the input inserted by a player or by the timer
  /// Then calculate player points and start a new match if nobody won
  void _saveInput() {
    var correct = lastInput?.correct;
    var playerId = lastInput?.player ?? 0;
    if (correct != null) playerList[playerId].points += correct ? 1 : -1;
    if (playerList[playerId].points == settings.requiredPoints ||
        (playerList[playerId].points == 0 - settings.requiredPoints &&
            playerList.length <= 2) ||
        roundCount == settings.maxRounds) {
      endGame();
    } else if (!ui.widget.randomMode) {
      newPhase(GamePhase.preMain, forced: true);
      if (autoStart) newTurn();
      ui.refresh();
    }
    if (ui.widget.randomMode && !isOver) {
      settings.retryUntilCorrect && (correct ?? false) == false
          ? changeGame(loadGame(this, id))
          : changeGame(nextRandomGame(this));
    }
    ui.refresh();
    lastInput = null;
  }

  /// Method called if someone won/lost
  void endGame() {
    newPhase(GamePhase.end);
    dispose();
  }

  /// Pause or un-pause the game
  @mustCallSuper
  void pause([PauseMenu? pauseMenu]) {
    if (!isPaused) {
      newPhase(GamePhase.pause);
      ui.pauseMenu = pauseMenu ?? PauseMenu.pause;
    } else {
      if (_phaseLog.length <= 1 ||
          _phaseLog[_phaseLog.length - 2] == GamePhase.preMain) {
        changeGame(loadGame(this, id));
      } else if (_phaseLog[_phaseLog.length - 2] == GamePhase.postMain) {
        _saveInput();
      } else {
        newPhase(_phaseLog[_phaseLog.length - 2], forced: true);
      }
      ui.pauseMenu = PauseMenu.none;
      ui.pausedBy = 0;
    }
  }

  /// Text shown if isPrePhase
  String get tip => info.subtitle;

  /// Text shown if isMain or isPostPhase
  String? get question => info.question;

  /// Text shown in the main widget
  String _mainText() {
    var text = '';
    if (isOver) {
      if (roundCount == settings.maxRounds &&
          ranking.first.points != settings.requiredPoints) {
        text = 'Too many rounds'.i18n + ' ($roundCount). ';
        if (!multiplayer) text += '\n' + 'You lost!'.i18n;
      } else if (ranking.first.points != settings.requiredPoints) {
        return 'Too many mistakes.'.i18n +
            ' ' +
            (!multiplayer
                ? '\n' + 'You lost!'.i18n
                : '%s won!'.i18n.fill([ranking.first.name]));
      }
      if (multiplayer) {
        if (ranking.first.points == 0) {
          text += 'Nobody won!'.i18n;
        } else {
          text += '%s won!'.i18n.fill([ranking.first.name]);
        }
      } else if (ranking.first.points == settings.requiredPoints) {
        text += 'You won!'.i18n;
      }
    } else if (isPaused) {
      if (ui.pauseMenu == PauseMenu.pause) {
        text = 'Pause'.i18n.toUpperCase();
      } else if (ui.pauseMenu == PauseMenu.exit) {
        text = 'Are you sure you want to quit this game?'.i18n;
      } else {
        text = 'Are you sure you want to restart this game?'.i18n;
      }
    } else if (isStarting) {
      text = info.title;
    } else {
      text = (isMainPhase || isPostPhase) ? question! : tip;
    }
    return text;
  }

  /// Game widget
  /// widget() should be overridden to implement custom UI
  Color backgroundColor = Colors.white;
  double get widgetHeight =>
      MediaQuery.of(ui.context).size.height / (multiplayer ? 2.5 : 2);
  Widget interface() => InkWell(
        onTap: isPrePhase || isMainPhase || isPostPhase ? pause : null,
        highlightColor: Colors.transparent,
        splashColor: Colors.transparent,
        hoverColor: Colors.transparent,
        child: Container(
          height: widgetHeight,
          width: MediaQuery.of(ui.context).size.width,
          color:
              isPrePhase || isMainPhase || isPostPhase ? backgroundColor : null,
          child: isStarting || isPaused || isOver ? _defaultWidget() : widget(),
        ),
      );
  Widget widget() => _defaultWidget();
  Widget _defaultWidget() {
    var textWidget = Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: Text(
          _mainText(),
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.grey[800], fontSize: min(26, widgetHeight / 9)),
        ),
      ),
    );
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        if (multiplayer) ...[
          RotatedBox(quarterTurns: 2, child: textWidget),
          const Divider(height: 1)
        ],
        textWidget,
      ],
    );
  }

  /// Widget with text on top and bottom of child
  double get innerWidgetHeight => widgetHeight / (multiplayer ? 1.4 : 1.2);
  Widget questionWidget(
      {required Widget child, String? text, bool? hasEmoji, Color? textColor}) {
    var textWidget = Padding(
      padding: const EdgeInsets.all(6),
      child: FittedBox(
        fit: BoxFit.fitWidth,
        child: (hasEmoji ?? false)
            ? EmojiText(
                text: text ?? question!,
                color: textColor ?? Colors.grey.shade800,
                fontWeight: FontWeight.w500,
              )
            : Text(
                text ?? question!,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: textColor ?? Colors.grey.shade800,
                  fontWeight: FontWeight.w500,
                ),
              ),
      ),
    );
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        if (multiplayer) ...[
          Expanded(
            child: RotatedBox(quarterTurns: 2, child: textWidget),
          ),
          Divider(height: 1, color: textColor),
        ],
        SizedBox(height: innerWidgetHeight, child: child),
        Divider(height: 1, color: textColor),
        Expanded(child: textWidget),
      ],
    );
  }

  /// Change active game
  void changeGame(Game newGame) {
    dispose();
    ui.game = newGame;
    ui.refresh();
  }

  /// Restart the game
  void restart() {
    changeGame((ui.widget.id ?? getRandomGame(ui.widget.category))
        .load(players: ui.widget.playerCount, gamePageState: ui));
  }

  /// Manage back button during a game
  Future<bool> exit() {
    if (roundCount == 0) return Future.value(true);
    pause();
    ui.refresh();
    return Future.value(false);
  }

  /// Condition used to know whether timer should be paused
  bool isTimerPaused() =>
      !isMainPhase || isPostPhase || lastInput?.correct != null;

  /// Dispose the game
  @mustCallSuper
  void dispose() => _phase = GamePhase.end;
}
