/// # Game phases:
/// - starting: (input = false and output = false)
///     - game is starting and it is showing the rules
/// - preMain (OPTIONAL): (input = false and output = true)
///     - game does not expect any input by players
///     - game shows something and/or makes sounds and/or makes vibrations
///     - this phase can be skipped by not implementing prePhase()
/// - main: (input = true and output = maybe)
///     - game expects input by players
///     - game could show something and/or make sounds and/or makes vibrations
/// - postMain: (input = false and output = false)
///     - inputs have been sent, the result is shown
/// - pause:
///     - game is paused
/// - end:
///     - game is over, nothing is going to happen anymore
enum GamePhase { starting, preMain, main, postMain, pause, end }
