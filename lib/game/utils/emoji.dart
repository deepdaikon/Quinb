import 'package:flutter/material.dart';

/// Animals used in some games
// i18n: 'cat'.i18n 'duck'.i18n 'dog'.i18n
enum Animal {
  cat('🐈'), // U+1F408
  duck('🦆'), // U+1F986
  dog('🐕'); // U+1F415

  const Animal(this.emoji);
  final String emoji;
  String get fileName => name + '_1.wav';
}

/// List of emoji
final List<String> emojis = [
  '🍉', // watermelon U+1F349
  '🥑', // avocado U+1F951
  '⚽', // football U+26BD
  '🎨', // palette U+1F3A8
  '🍀', // clover U+1F340
  '🌈', // rainbow U+1F308
  '🔥', // fire U+1F525
  '❄', // snowflake U+2744
  '❤', // heat U+2764
  '💾', // floppy disk U+1F4BE
  '🔶', // orange diamond U+1F536
  '🔔', // bell U+1F514
  '✏', // pencil U+270F
  '📎', // paperclip U+1F4CE
  '🧬', // dna U+1F9EC
  '🗿', // moai U+1F5FF
  '⚓', // anchor U+2693
  '🥦', // broccoli U+1F966
  '🍄', // mushroom U+1F344
  '🐒', // monkey U+1F412
  '🦄', // unicorn U+1F984
  '🐖', // sunflower U+1F416
  '🐐', // goat U+1F410
  '🐇', // rabbit U+1F407
  '🐀', // rat U+1F400
  '🐤', // chick U+1F424
  '🌻', // sunflower U+1F33B
  ...Animal.values.map((e) => e.emoji)
];

final RegExp _emojiRegex = RegExp(
    r'\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff]');
bool hasEmoji(String text) => _emojiRegex.hasMatch(text);

/// Text widget with emoji. Used for texts with both normal characters and emoji.
/// Waiting for something like https://github.com/flutter/flutter/issues/84631
class EmojiText extends StatelessWidget {
  EmojiText(
      {required this.text, required this.color, required this.fontWeight});
  final String text;
  final Color color;
  final FontWeight fontWeight;

  @override
  Widget build(BuildContext context) => RichText(text: _buildText());
  TextSpan _buildText() {
    final children = <TextSpan>[];
    final runes = text.runes;
    for (var i = 0; i < runes.length;) {
      var current = runes.elementAt(i);
      final bool isEmoji = hasEmoji(String.fromCharCode(current));
      final shouldBreak = isEmoji ? (c) => !hasEmoji(c) : hasEmoji;

      final List<int> chunk = [];
      while (!shouldBreak(String.fromCharCode(current))) {
        chunk.add(current);
        if (++i >= runes.length) break;
        current = runes.elementAt(i);
      }
      children.add(
        TextSpan(
          text: String.fromCharCodes(chunk),
          style: TextStyle(
            color: color,
            fontWeight: fontWeight,
            fontFamily: isEmoji ? 'emoji' : null,
          ),
        ),
      );
    }
    return TextSpan(children: children);
  }
}
