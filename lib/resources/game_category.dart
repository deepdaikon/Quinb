import 'package:flutter/material.dart';

// i18n: 'Logic'.i18n 'Sound'.i18n 'Vibration'.i18n
// ignore_for_file: constant_identifier_names
enum GameCategory {
  Logic(Icons.category_rounded),
  Sound(Icons.volume_up_rounded),
  Vibration(Icons.vibration_rounded);

  const GameCategory(this.icon);
  final IconData icon;
}
