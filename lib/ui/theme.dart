import 'package:flutter/material.dart';

/// App theme
final theme = ThemeData(
  colorSchemeSeed: const Color(0xEE204569),
  dividerColor: Colors.grey.shade300,
  scaffoldBackgroundColor: Colors.white,
  appBarTheme: const AppBarTheme(
      titleTextStyle: TextStyle(
          letterSpacing: 6, fontSize: 22, fontWeight: FontWeight.w600)),
);
