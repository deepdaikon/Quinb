import 'package:flutter/material.dart';
import 'package:quinb/ui/screens/info/resources/third_party_licenses.dart';
import 'package:quinb/utils/i18n.dart';

/// Display third party licenses in an alert dialog
Future licenseDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Third Party Licenses'.i18n),
          content: SizedBox(
            width: double.maxFinite,
            child: ListView.builder(
              itemCount: licenses.length,
              itemBuilder: (BuildContext context, int index) {
                return ExpansionTile(
                  title: Text(licenses[index]['lib']!),
                  initiallyExpanded: true,
                  children: <Widget>[
                    SingleChildScrollView(
                        child: Text(licenses[index]['text']!)),
                  ],
                );
              },
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: Navigator.of(context).pop,
              child: const Text('Ok', style: TextStyle(color: Colors.black)),
            ),
          ],
        );
      },
    );
