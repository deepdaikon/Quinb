import 'package:flutter/material.dart';
import 'package:quinb/ui/screens/info/widgets/license_dialog.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:url_launcher/url_launcher.dart';

class InfoPage extends StatefulWidget {
  @override
  State<InfoPage> createState() => _InfoPageState();
}

class _InfoPageState extends State<InfoPage> {
  static const _appVersion = '1.2.5';
  final infoMenuList = <Map<String, dynamic>>[
    {
      'title': 'DeepDaikon Project',
      'subtitle': 'App developed by %s'.i18n.fill(['DeepDaikon']),
      'url': 'https://deepdaikon.xyz',
      'icon': const Icon(Icons.change_history_rounded),
    },
    {
      'title': 'Version:'.i18n + ' $_appVersion',
      'subtitle': 'App version'.i18n,
      'icon': const Icon(Icons.looks_one_rounded),
    },
    {
      'title': 'Donate'.i18n,
      'subtitle': 'Support the development'.i18n,
      'url': 'https://deepdaikon.xyz/donate',
      'icon': const Icon(Icons.euro_symbol_rounded),
    },
    {
      'title': 'Translate'.i18n,
      'subtitle': 'Translate in your language'.i18n,
      'url': 'https://translate.deepdaikon.xyz/engage/quinb/',
      'icon': const Icon(Icons.translate_rounded),
    },
    {
      'title': 'Send email'.i18n,
      'subtitle': 'Ask for something or request a new feature'.i18n,
      'url': 'mailto:deepdaikon' '@' 'tuta.io?subject=Quinb Game',
      'icon': const Icon(Icons.email),
    },
    {
      'title': 'Report bugs'.i18n,
      'subtitle': 'Report bugs or request new feature'.i18n,
      'url': 'https://gitlab.com/deepdaikon/Quinb/issues',
      'icon': const Icon(Icons.bug_report_rounded),
    },
    {
      'title': 'View source code'.i18n,
      'subtitle': 'Look at the source code'.i18n,
      'url': 'https://gitlab.com/deepdaikon/Quinb',
      'icon': const Icon(Icons.developer_mode_rounded),
    },
    {
      'title': 'View License'.i18n,
      'subtitle': 'Read software license'.i18n,
      'url': 'https://gitlab.com/deepdaikon/Quinb/blob/master/LICENSE',
      'icon': const Icon(Icons.chrome_reader_mode_rounded),
    },
    {
      'title': 'Third Party Licenses'.i18n,
      'subtitle': 'Read third party notices'.i18n,
      'icon': const Icon(Icons.code_rounded),
    }
  ];

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar:
            AppBar(title: Text('Info'.i18n.toUpperCase()), centerTitle: true),
        body: ListView.builder(
          padding: const EdgeInsets.all(8),
          itemCount: infoMenuList.length,
          itemBuilder: (BuildContext context, int index) {
            return ListTile(
              leading: Icon(infoMenuList[index]['icon'].icon, size: 27),
              title: Text(
                infoMenuList[index]['title'],
                style: const TextStyle(fontSize: 20),
              ),
              subtitle: Text(infoMenuList[index]['subtitle']),
              onTap: () {
                if (infoMenuList[index].containsKey('url')) {
                  launchUrl(Uri.parse(infoMenuList[index]['url']),
                      mode: LaunchMode.externalApplication);
                } else if (infoMenuList[index]['title'] ==
                    'Third Party Licenses'.i18n) {
                  licenseDialog(context);
                }
              },
            );
          },
        ),
      );
}
