import 'package:flutter/material.dart';
import 'package:quinb/resources/game_category.dart';
import 'package:quinb/resources/game_list.dart';
import 'package:quinb/ui/basic.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Floating action button that start random games
/// It is rebuilt when selectedCategoryIndex changes
class Fab extends StatelessWidget {
  Fab(this.selectedCategoryIndex)
      : gamesInCategory = selectedCategoryIndex > 0 &&
            enabledGames.where((game) {
              return game.category ==
                  GameCategory.values[selectedCategoryIndex - 1];
            }).isEmpty;
  final int selectedCategoryIndex;
  final bool gamesInCategory;

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      elevation: 0,
      backgroundColor: gamesInCategory
          ? Colors.blueGrey[600]
          : Theme.of(context).primaryColor,
      tooltip: 'Random'.i18n,
      onPressed: gamesInCategory
          ? null
          : () {
              Navigator.push(
                context,
                FadeRoute(
                  GamePage(
                    playerCount: settings.players,
                    category: selectedCategoryIndex != 0
                        ? GameCategory.values[selectedCategoryIndex - 1]
                        : null,
                  ),
                ),
              );
            },
      child: const Icon(Icons.shuffle_rounded),
    );
  }
}
