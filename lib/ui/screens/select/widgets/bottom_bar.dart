import 'package:flutter/material.dart';
import 'package:quinb/resources/game_category.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Bottom bar with categories
Widget bottomBar(BuildContext context, int category, Function callback) {
  return BottomAppBar(
    child: Container(
      height: 56,
      decoration: BoxDecoration(
          border: Border(
              top:
                  BorderSide(color: Theme.of(context).primaryColor, width: 3))),
      child: Container(
        margin: const EdgeInsetsDirectional.only(end: 75),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            for (int i = -1; i < GameCategory.values.length; i++)
              if ((settings.enableSoundGames ||
                      (i == -1 ||
                          GameCategory.values[i] != GameCategory.Sound)) &&
                  (settings.enableVibrationGames ||
                      (i == -1 ||
                          GameCategory.values[i] != GameCategory.Vibration)))
                Flexible(
                  flex: (category != i + 1) ? 1 : 2,
                  child: InkWell(
                    onTap: () => callback(i),
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    child: Center(
                      child: (category != i + 1)
                          ? Icon(
                              i == -1
                                  ? Icons.scatter_plot_rounded
                                  : GameCategory.values[i].icon,
                              color: Colors.black87,
                            )
                          : Text(
                              i == -1
                                  ? 'All'.i18n
                                  : GameCategory.values[i].name.i18n,
                              maxLines: 1,
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w900,
                                color: Theme.of(context)
                                    .primaryColor
                                    .withOpacity(0.7),
                              ),
                            ),
                    ),
                  ),
                ),
          ],
        ),
      ),
    ),
  );
}
