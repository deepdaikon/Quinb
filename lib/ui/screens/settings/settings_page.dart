import 'dart:io';

import 'package:flutter/material.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:quinb/resources/game_list.dart';
import 'package:quinb/ui/basic.dart';
import 'package:quinb/ui/screens/settings/widgets/disable_game_dialog.dart';
import 'package:quinb/ui/screens/settings/widgets/number_picker_tile.dart';
import 'package:quinb/ui/screens/settings/widgets/restore_dialog.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';
import 'package:quinb/utils/settings.dart';

class SettingsPage extends StatefulWidget {
  @override
  SettingsPageState createState() => SettingsPageState();
}

class SettingsPageState extends State<SettingsPage> {
  Map<String, bool> expanded = {};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'.i18n.toUpperCase()),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.settings_backup_restore_rounded),
            tooltip: 'Restore'.i18n,
            onPressed: () => restoreSettingsDialog(context, setState),
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            title:
                Text('Difficulty'.i18n, style: const TextStyle(fontSize: 20)),
            subtitle: Text('Difficulty of the games'.i18n),
            trailing: DropdownButton<Difficulty>(
              value: settings.difficulty,
              onChanged: (newValue) {
                setState(() => settings.difficulty = newValue!);
                saveSettings();
              },
              items: Difficulty.values
                  .map<DropdownMenuItem<Difficulty>>(
                    (Difficulty value) => DropdownMenuItem<Difficulty>(
                      value: value,
                      child: Text(value.name.i18n),
                    ),
                  )
                  .toList(),
            ),
          ),
          CheckboxListTile(
              title: Text('Sound games'.i18n,
                  style: const TextStyle(fontSize: 20)),
              subtitle: Text(soundToggleText),
              value: settings.enableSoundGames,
              onChanged: (newValue) {
                setState(() => settings.enableSoundGames = newValue!);
                saveSettings();
              }),
          if (Platform.isAndroid)
            CheckboxListTile(
                title: Text('Vibration games'.i18n,
                    style: const TextStyle(fontSize: 20)),
                subtitle: Text(vibrationToggleText),
                value: settings.enableVibrationGames,
                onChanged: (newValue) {
                  setState(() => settings.enableVibrationGames = newValue!);
                  saveSettings();
                }),
          numberPickerTile(
            context,
            setState,
            expanded,
            itemId: 'requiredPoints',
            title: 'Points required'.i18n,
            subtitle: 'Points required to win'.i18n,
            minValue: 1,
            maxValue: 25,
            getValue: () => settings.requiredPoints,
            callback: (newValue) => settings.requiredPoints = newValue,
          ),
          numberPickerTile(
            context,
            setState,
            expanded,
            itemId: 'timeAvailable',
            title: 'Round duration'.i18n,
            subtitle: 'Time available each round (in seconds)'.i18n,
            minValue: 5,
            maxValue: 15,
            getValue: () => settings.timeAvailable,
            callback: (newValue) => settings.timeAvailable = newValue,
          ),
          numberPickerTile(
            context,
            setState,
            expanded,
            itemId: 'maxRounds',
            title: 'Rounds'.i18n,
            subtitle: 'Maximum number of rounds'.i18n,
            minValue: 1,
            maxValue: 99,
            getValue: () => settings.maxRounds,
            callback: (newValue) => settings.maxRounds = newValue,
          ),
          numberPickerTile(
            context,
            setState,
            expanded,
            itemId: 'waitingTime',
            title: 'Waiting time'.i18n,
            subtitle: 'Waiting time before a match (in seconds)'.i18n,
            minValue: 1,
            maxValue: 15,
            getValue: () => settings.waitingTime,
            callback: (newValue) => settings.waitingTime = newValue,
          ),
          CheckboxListTile(
              title: Text('Sound effects'.i18n,
                  style: const TextStyle(fontSize: 20)),
              subtitle: Text('Play sound effects during the game'.i18n),
              value: settings.sfx,
              onChanged: (newValue) {
                setState(() => settings.sfx = newValue!);
                saveSettings();
              }),
          CheckboxListTile(
              title: Text('24-hour clock'.i18n,
                  style: const TextStyle(fontSize: 20)),
              subtitle: Text('Use the 24-hour time notation in games'.i18n),
              value: settings.use24h,
              onChanged: (newValue) {
                setState(() => settings.use24h = newValue!);
                saveSettings();
              }),
          CheckboxListTile(
              title: Text('Show actions on loading'.i18n,
                  style: const TextStyle(fontSize: 20)),
              subtitle: Text(
                  'Show skip button and play button in the loading page'.i18n),
              value: settings.showActionsOnLoading,
              onChanged: (newValue) {
                setState(() => settings.showActionsOnLoading = newValue!);
                saveSettings();
              }),
          CheckboxListTile(
              title: Text('Retry until correct'.i18n,
                  style: const TextStyle(fontSize: 20)),
              subtitle: Text(
                  'Try again mini-games until someone gives a correct answer'
                      .i18n),
              value: settings.retryUntilCorrect,
              onChanged: (newValue) {
                setState(() => settings.retryUntilCorrect = newValue!);
                saveSettings();
              }),
          ListTile(
              title: Text('Disabled games'.i18n,
                  style: const TextStyle(fontSize: 20)),
              subtitle:
                  Text("Select the games you don't want to play anymore".i18n),
              onTap: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) =>
                      const StatefulBuilder(builder: disableGameDialog),
                );
              }),
          ListTile(
            title: Text('Language'.i18n, style: const TextStyle(fontSize: 20)),
            subtitle: Text('App language'.i18n),
            trailing: DropdownButton<Locale>(
              value: settings.useSystemLanguage ? null : settings.locale,
              onChanged: (newValue) {
                setState(() {
                  settings.locale = newValue;
                  I18n.of(context).locale = settings.locale;
                  reloadGameList();
                  saveSettings();
                });
              },
              items: [
                DropdownMenuItem<Locale>(
                  value: null,
                  child: Text('System default'.i18n),
                ),
                for (final locale in supportedLocales)
                  DropdownMenuItem<Locale>(
                    value: locale,
                    child: Text(languageNames.containsKey(locale.languageCode)
                        ? languageNames[locale.languageCode]![1]
                        : 'missing name'),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
