import 'package:flutter/material.dart';
import 'package:quinb/resources/game_category.dart';
import 'package:quinb/resources/game_list.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Dialog used to disable games
Widget disableGameDialog(BuildContext context, StateSetter setState) {
  return AlertDialog(
    title: Text('Disabled games'.i18n),
    content: SizedBox(
      width: double.maxFinite,
      child: ListView.builder(
        itemCount: gameList.length,
        itemBuilder: (BuildContext context, int index) {
          return CheckboxListTile(
            title: Text(gameList[index].title),
            value: settings.disabledGames.contains(gameList[index].id.name),
            onChanged: (enabled) {
              if (enabled!) {
                settings.disabledGames.add(gameList[index].id.name);
                if (enabledGames
                    .where((game) => game.category == GameCategory.Logic)
                    .isEmpty) {
                  settings.disabledGames.remove(gameList[index].id.name);
                }
              } else {
                settings.disabledGames.remove(gameList[index].id.name);
              }
              saveSettings();
              setState(() {});
            },
          );
        },
      ),
    ),
    actions: <Widget>[
      TextButton(onPressed: Navigator.of(context).pop, child: Text('Ok'.i18n)),
    ],
  );
}
