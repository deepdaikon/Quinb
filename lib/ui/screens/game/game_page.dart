import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:quinb/resources/game_category.dart';
import 'package:quinb/resources/game_loader.dart';
import 'package:quinb/ui/screens/game/widgets/game_feedback.dart';
import 'package:quinb/ui/screens/game/widgets/option_list.dart';
import 'package:quinb/ui/screens/game/widgets/pause_menu.dart';
import 'package:quinb/ui/screens/game/widgets/play_timer_buttons.dart';
import 'package:quinb/ui/screens/game/widgets/player_zone.dart';
import 'package:quinb/ui/screens/game/widgets/timer.dart';

class GamePage extends StatefulWidget {
  GamePage({required this.playerCount, this.id, this.category})
      : randomMode = id == null;

  /// True if should change mini-game every round
  final bool randomMode;

  /// Game configurations
  final int playerCount;
  final GameId? id;
  final GameCategory? category;

  @override
  GamePageState createState() => GamePageState();
}

class GamePageState extends State<GamePage>
    with GameFeedback, TickerProviderStateMixin, WidgetsBindingObserver {
  OptionList optionList = OptionList();
  PauseMenu pauseMenu = PauseMenu.none;
  int pausedBy = 0;

  @override
  void initState() {
    game = (widget.id ?? (getRandomGame(widget.category)))
        .load(players: widget.playerCount, gamePageState: this);
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: game!.exit,
      child: Scaffold(
        body: AnnotatedRegion(
          value: const SystemUiOverlayStyle(
              statusBarIconBrightness: Brightness.dark),
          sized: false,
          child: SafeArea(
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    if (game!.multiplayer)
                      PlayerZone(game!,
                          (int playerId) => optionList.options(game!, playerId),
                          upsideDown: true),
                    game!.interface(),
                    PlayerZone(game!,
                        (int playerId) => optionList.options(game!, playerId),
                        upsideDown: false),
                  ],
                ),
                if (game!.isMainPhase)
                  TimerWidget(game!)
                else if (game!.isStarting)
                  PlayTimerButtons(game!)
                else if (game!.isPostPhase)
                  resultPopup(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void refresh() {
    if (mounted) setState(() {});
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state != AppLifecycleState.resumed && !game!.isPaused) game?.pause();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    game!.dispose();
    game = null;
    super.dispose();
  }
}
