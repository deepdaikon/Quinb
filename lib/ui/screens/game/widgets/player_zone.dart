import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/ui/basic.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Widget with points and options for one or two players
class PlayerZone extends StatefulWidget {
  PlayerZone(this.game, this.optionList, {required this.upsideDown});
  final Game game;
  final bool upsideDown;
  final Function optionList;

  @override
  State<PlayerZone> createState() => _PlayerZoneState();
}

class _PlayerZoneState extends State<PlayerZone> {
  final scrollController = ScrollController(initialScrollOffset: -100);

  @override
  Widget build(BuildContext context) {
    if (scrollController.hasClients) {
      scrollController.jumpTo(scrollController.initialScrollOffset);
    }
    return Expanded(
      child: RotatedBox(
        quarterTurns: widget.upsideDown ? 2 : 0,
        child: widget.game.isStarting
            ? Container(
                color: Theme.of(context).primaryColor,
                child: Stack(
                  children: [
                    ScrollConfiguration(
                      behavior: NoScrollGlow(),
                      child: SingleChildScrollView(
                        controller: scrollController,
                        primary: false,
                        child: Container(
                          width:
                              MediaQuery.of(widget.game.ui.context).size.width,
                          margin: const EdgeInsets.fromLTRB(20, 40, 20, 75),
                          child: Text(widget.game.info.rules,
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                  color: Colors.white, fontSize: 20)),
                        ),
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(child: points(widget.upsideDown ? 1 : 0)),
                        if (widget.game.playerList.length >
                            (widget.upsideDown ? 2 : 3))
                          Expanded(child: points(widget.upsideDown ? 2 : 3)),
                      ],
                    ),
                  ],
                ),
              )
            : Container(
                color: Colors.grey.shade200,
                child: Row(
                  children: <Widget>[
                    Expanded(child: options(widget.upsideDown ? 1 : 0)),
                    if (widget.game.playerList.length >
                        (widget.upsideDown ? 2 : 3))
                      Expanded(child: options(widget.upsideDown ? 2 : 3)),
                  ],
                ),
              ),
      ),
    );
  }

  Widget points(int playerId) => Container(
        height: 18,
        color: Colors.grey[200],
        child: Center(
          child: Text(
              '${widget.game.playerList[playerId].points} / ${settings.requiredPoints} ' +
                  'points'.i18n),
        ),
      );

  Widget options(int id) =>
      Column(children: [points(id), ...widget.optionList(id)]);
}
