import 'package:flutter/material.dart';
import 'package:quinb/game/game.dart';
import 'package:quinb/resources/game_loader.dart';
import 'package:quinb/ui/screens/game/widgets/pause_menu.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';

/// Floating play / pause buttons with timer
class PlayTimerButtons extends StatefulWidget {
  PlayTimerButtons(this.game);
  final Game game;

  @override
  State<PlayTimerButtons> createState() => _PlayTimerButtonsState();
}

class _PlayTimerButtonsState extends State<PlayTimerButtons>
    with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  late AnimationController controller;

  @override
  void initState() {
    controller = AnimationController(
      vsync: this,
      // if the game is resumed on prePhase: duration = 2 seconds
      duration: Duration(
          seconds:
              widget.game.roundCount != 0 && !widget.game.ui.widget.randomMode
                  ? 2
                  : settings.waitingTime),
      upperBound: 120,
    )..forward();
    controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) widget.game.startGame();
    });
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        if (widget.game.multiplayer)
          Align(
            alignment: Alignment.topCenter,
            child: RotatedBox(quarterTurns: 2, child: buttonsWidget(true)),
          ),
        Align(alignment: Alignment.bottomCenter, child: buttonsWidget(false)),
      ],
    );
  }

  // ignore: avoid_positional_boolean_parameters
  Widget buttonsWidget(bool upsideDown) {
    return Container(
      color: Theme.of(context).primaryColor,
      padding: const EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          if (!controller.isAnimating)
            IconButton(
              onPressed: () {
                controller.stop();
                widget.game.pause(PauseMenu.exit);
                widget.game.ui.pausedBy = upsideDown ? 1 : 0;
                widget.game.ui.refresh();
              },
              icon: const Icon(Icons.close_rounded,
                  color: Colors.white, size: 30),
            )
          else if (settings.showActionsOnLoading)
            widget.game.ui.widget.randomMode
                ? IconButton(
                    onPressed: () {
                      controller
                          .fling(velocity: -1)
                          .then((_) => controller.forward());
                      widget.game.changeGame(nextRandomGame(widget.game));
                      widget.game.ui.refresh();
                    },
                    icon: const Icon(Icons.redo_rounded,
                        color: Colors.white, size: 30),
                  )
                : Container(
                    padding: const EdgeInsets.all(8),
                    margin: const EdgeInsets.all(8),
                    width: 32,
                    height: 32,
                  ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              InkWell(
                enableFeedback: false,
                child: SizedBox(
                  width: 150,
                  height: 50,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Center(
                      child: Text(
                        (controller.isAnimating ? 'Pause'.i18n : 'Resume'.i18n)
                            .toUpperCase(),
                        style: const TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w800,
                            letterSpacing: 3),
                      ),
                    ),
                  ),
                ),
                onTap: () => setState(() {
                  controller.isAnimating
                      ? controller.stop()
                      : controller.forward();
                }),
              ),
              AnimatedBuilder(
                animation: CurvedAnimation(
                    parent: controller, curve: Curves.fastLinearToSlowEaseIn),
                builder: (context, child) => Container(
                  width: controller.upperBound - controller.value,
                  height: 2,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              ),
            ],
          ),
          if (!controller.isAnimating)
            IconButton(
              onPressed: () {
                controller.stop();
                widget.game.pause(PauseMenu.restart);
                widget.game.ui.pausedBy = upsideDown ? 1 : 0;
                widget.game.ui.refresh();
              },
              icon: const Icon(Icons.replay_rounded,
                  color: Colors.white, size: 26),
            )
          else if (settings.showActionsOnLoading)
            IconButton(
              onPressed: controller.fling,
              icon: const Icon(Icons.play_arrow_rounded,
                  color: Colors.white, size: 30),
            ),
        ],
      ),
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setState(() {
      controller.stop();
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    controller.dispose();
    super.dispose();
  }
}
