import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_volume_controller/flutter_volume_controller.dart';
import 'package:quinb/resources/game_list.dart';
import 'package:quinb/ui/basic.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/ui/screens/home/widgets/page_button.dart';
import 'package:quinb/ui/screens/info/info_page.dart';
import 'package:quinb/ui/screens/rules/rules_page.dart';
import 'package:quinb/ui/screens/select/select_page.dart';
import 'package:quinb/ui/screens/settings/settings_page.dart';
import 'package:quinb/utils/i18n.dart';
import 'package:quinb/utils/local_data_controller.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  List<Map<String, dynamic>> pageList() {
    return [
      {
        'title': 'Play'.i18n,
        'goto': () => GamePage(playerCount: settings.players)
      },
      {'title': 'Select game'.i18n, 'goto': SelectPage.new},
      {'title': 'Settings'.i18n, 'goto': SettingsPage.new},
    ];
  }

  /// Title rotation animation
  late AnimationController rotationController;
  late Animation<double> animation;

  /// Key of the scaffold of this page
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    reloadGameList();
    rotationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
    animation = Tween<double>(begin: 0, end: pi).animate(rotationController);
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      var volume = await FlutterVolumeController.getVolume();
      var muted = (await FlutterVolumeController.getMute()) ?? false;
      if (settings.enableVolumeOnStart == null &&
          (volume == 0 || muted) &&
          settings.enableSoundGames) {
        await volumeDialog().then((_) => mounted ? setState(() {}) : null);
      } else if ((volume == 0 || muted) && settings.enableSoundGames) {
        if (settings.enableVolumeOnStart!) {
          FlutterVolumeController.setMute(false);
          FlutterVolumeController.setVolume(0.5);
        } else {
          snackBar(_scaffoldKey);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: AnnotatedRegion(
        value: const SystemUiOverlayStyle(
            statusBarIconBrightness: Brightness.light),
        sized: false,
        child: InkWell(
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
          onTap: () {
            setState(() {
              rotationController.isCompleted
                  ? rotationController.reverse()
                  : rotationController.forward();
            });
          },
          child: DecoratedBox(
            decoration: const BoxDecoration(gradient: appGradient),
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Center(
                    child: AnimatedBuilder(
                      animation: animation,
                      child: const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 30),
                        child: FittedBox(
                          fit: BoxFit.fitWidth,
                          child: Text(
                            '¿ quinb ?',
                            style: TextStyle(
                              letterSpacing: 5,
                              fontSize: 80,
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.italic,
                            ),
                          ),
                        ),
                      ),
                      builder: (context, child) => Transform.rotate(
                          angle: animation.value, child: child),
                    ),
                  ),
                ),
                for (var page in pageList())
                  PageButton(
                    title: page['title'],
                    onPressed: () =>
                        Navigator.push(context, FadeRoute(page['goto']()))
                            .then((_) => setState(() {})),
                  ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                          settings.enableSoundGames
                              ? Icons.volume_up_rounded
                              : Icons.volume_off_rounded,
                          color: Colors.white.withOpacity(0.7),
                          size: 28),
                      tooltip: 'Sound games'.i18n,
                      onPressed: () {
                        setState(() {
                          settings.enableSoundGames =
                              !settings.enableSoundGames;
                          saveSettings();
                          snackBar(_scaffoldKey, text: soundToggleText);
                        });
                      },
                    ),
                    IconButton(
                      icon: Icon(
                          settings.enableVibrationGames
                              ? Icons.vibration_rounded
                              : Icons.mobile_off_rounded,
                          color: Colors.white.withOpacity(0.7),
                          size: 28),
                      tooltip: 'Vibration games'.i18n,
                      onPressed: Platform.isAndroid
                          ? () {
                              setState(() {
                                settings.enableVibrationGames =
                                    !settings.enableVibrationGames;
                                saveSettings();
                                snackBar(_scaffoldKey,
                                    text: vibrationToggleText);
                              });
                            }
                          : () => snackBar(_scaffoldKey,
                              text: vibrationNotSupported),
                    ),
                    const Spacer(),
                    DropdownButton<int>(
                      value: settings.players,
                      underline: Container(),
                      focusColor: Colors.transparent,
                      selectedItemBuilder: (BuildContext context) {
                        return [1, 2, 3, 4]
                            .map<Widget>((int value) => Center(
                                  child: Text(
                                    '$value ' +
                                        (value > 1
                                            ? 'players'.i18n
                                            : 'player'.i18n),
                                    textAlign: TextAlign.center,
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 22,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ))
                            .toList();
                      },
                      icon: Container(),
                      onChanged: (newValue) {
                        setState(() {
                          settings.players = newValue!;
                          saveSettings();
                        });
                      },
                      items: [1, 2, 3, 4]
                          .map<DropdownMenuItem<int>>(
                            (int value) => DropdownMenuItem<int>(
                              value: value,
                              child: Text(
                                '$value ' +
                                    (value > 1
                                        ? 'players'.i18n
                                        : 'player'.i18n),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontWeight: value == settings.players
                                      ? FontWeight.bold
                                      : FontWeight.normal,
                                ),
                              ),
                            ),
                          )
                          .toList(),
                    ),
                    const Spacer(),
                    IconButton(
                      icon: Icon(Icons.help_outline_rounded,
                          color: Colors.white.withOpacity(0.7), size: 28),
                      tooltip: 'How to play'.i18n,
                      onPressed: () {
                        Navigator.push(context, FadeRoute(RulesPage()));
                      },
                    ),
                    IconButton(
                      icon: Icon(Icons.info_outline_rounded,
                          color: Colors.white.withOpacity(0.7), size: 28),
                      tooltip: 'Info'.i18n,
                      onPressed: () {
                        Navigator.push(context, FadeRoute(InfoPage()));
                      },
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  // Dialog displayed if volume == 0
  Future<void> volumeDialog() {
    var rememberChoice = false;
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (context, setState) {
            return WillPopScope(
              onWillPop: () {
                snackBar(_scaffoldKey);
                return Future.value(true);
              },
              child: AlertDialog(
                title: Text(
                  'Enable volume?'.i18n,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Theme.of(context).colorScheme.secondary),
                ),
                content: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Text(
                          '\n' +
                              'It looks like you have audio muted.\n\nYou should enable it in order to play sound-based games.'
                                  .i18n +
                              '\n',
                          style: const TextStyle(fontSize: 18)),
                      InkWell(
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          child: Row(
                            children: <Widget>[
                              Checkbox(
                                  value: rememberChoice,
                                  onChanged: (newValue) {
                                    setState(() {
                                      rememberChoice = newValue!;
                                    });
                                  }),
                              Text('Remember choice'.i18n),
                            ],
                          ),
                          onTap: () {
                            setState(() {
                              rememberChoice = !rememberChoice;
                            });
                          }),
                    ],
                  ),
                ),
                elevation: 0,
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                actions: <Widget>[
                  Center(
                    child: TextButton(
                      child: Text(
                        'Cancel'.i18n,
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 16,
                        ),
                      ),
                      onPressed: () {
                        if (rememberChoice) {
                          settings.enableVolumeOnStart = false;
                          saveSettings();
                        }
                        snackBar(_scaffoldKey);
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  Center(
                    child: TextButton(
                      child: Text(
                        'Disable audio games'.i18n,
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 16,
                        ),
                      ),
                      onPressed: () {
                        if (rememberChoice) {
                          settings.enableVolumeOnStart = false;
                        }
                        settings.enableSoundGames = false;
                        saveSettings();
                        snackBar(_scaffoldKey, text: soundToggleText);
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  Center(
                    child: TextButton(
                      child: Text(
                        'Enable volume'.i18n,
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 16,
                        ),
                      ),
                      onPressed: () {
                        FlutterVolumeController.setMute(false);
                        FlutterVolumeController.setVolume(0.5);
                        if (rememberChoice) {
                          settings.enableVolumeOnStart = true;
                          saveSettings();
                        }
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ],
              ),
            );
          });
        });
  }
}
